from django.shortcuts import render
from lab_2.views import landing_page_content
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message


# Create your views here.
response = {'author': "Samuel Tupa Febrian"} #TODO Implement yourname
about_me = ['I am 19 years old','Currently studying at University of Indonesia','what the hell am I doing rn?',
            'What should I do','I dunno anymore','*cries in the corner*']
errors = {'submitted' : False, 'warnings' : {}}
errorEmail = 'Please enter a valid email ( with @ )'
errorMesBox = 'Please enter a message ( at least 1 letter )'
errorName = 'Please enter a shorten name ( max 27 chars )'

def index(request):
    response['content'] = landing_page_content
    html = 'lab_4/lab_4.html'
        #TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    response['about_me'] = about_me
    response['message_form'] = Message_Form
    response['errors']= errors['warnings']
    if errors['submitted']:
        errors['submitted'] = False
    else:
        errors['warnings'] = {}
    print(errors['warnings'])
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):

        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'],
                          message=response['message'])
        message.save()
        html ='lab_4/form_result.html'
        return render(request, html, response)
    else:
        errors['submitted'] = True
        if len(request.POST['message'])==0:
            errors['warnings']['errorMes'] = errorMesBox
        if len(request.POST['email'])!=0 and '@' not in request.POST['email']:
            errors['warnings']['errorMail'] = errorEmail   
        if len(request.POST['name'])>27:
            errors['warnings']['errorName'] = errorName  
        return HttpResponseRedirect('/lab-4/')
def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'lab_4/table.html'
    return render(request, html , response)

